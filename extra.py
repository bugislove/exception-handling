try:
    # Code that may raise exceptions
    num = int(input("Enter a number: "))
    result = 10 / num
    print("Result:", result)

except Exception as e:
    print("An error occured:", str(e))