'''
- Incorrect Calculation
Code to calculate the average of 2 problems: 
'''

num1 = 10
num2 = 20

average_result = num1 + num2 / 2

print(f"Average of {num1} and {num2} is {average_result}")